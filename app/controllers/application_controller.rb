class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :layout_for_page
  helper_method :entries
  helper_method :nav
  helper_method :breadcrumbs
  helper_method :image

  def entries(path = nil, include_self = false, &block_argument)
    if block_argument # block is given
      entries(path, include_self).each do |p|
        block_argument.call(p) # use call to execute the block
      end
    else
      if path.nil?
        page_finder.list(include_self)
      else
        page_finder_factory.new(path).list(include_self)
      end
    end
  end

  def image(src = nil, options = {}, &block_argument)
    density = 1
    density ||= options[:density]

    width ||= options[:width]
    height ||= options[:height]

    #w = width * density
    #h = height * density

    if block_argument # block is given
      block_argument.call(OpenStruct.new({url: src, title: src, src: src,width: options[:width],height: options[:height]})) # use call to execute the block
    else
      "<img src=\"#{src}\" width=\"#{width.to_s}\" height=\"#{height.to_s}\">".html_safe
    end
  end



  def nav(path = nil, options = {}, &block_argument)
    path = entries("/", true) if path.to_s == "root" || path == nil
    if path.is_a?(Hash) || path.is_a?(OpenStruct)
      path = [path] 
      options[:levels]   ||= 2
    end
    path = entries(path) unless path.is_a? Array
    if block_argument # block is given
      path.each do |p|
        block_argument.call(p) # use call to execute the block
      end
    else # the value of block_argument becomes nil if you didn't give a block
      options[:levels]   ||= 1
      options[:level]    ||= 1
      options[:exclude]  ||= []
      options[:field]    ||= :title
      options[:exclude] = [options[:exclude].url] if options[:exclude].is_a? Hash
      options[:exclude]    = [options[:exclude]] if options[:exclude] && !options[:exclude].is_a?(Array)

      if options[:class].is_a?(Array)
        ss = "<ul#{ " class=\"" + options[:class].join(" ") + "\"" if options[:class] }>"
      elsif options[:class].is_a?(Hash)
        
        if options[:class][options[:level]]
          if options[:class][options[:level]].is_a?(Array)
            ss = "<ul#{ " class=\"" + options[:class][options[:level]].join(" ") + "\"" if options[:class][options[:level]] }>"
          else
            ss = "<ul#{ " class=\"" + options[:class][options[:level]].to_s + "\"" if options[:class][options[:level]] }>"
          end
        else
          ss = "<ul>"
        end
      else
        ss = "<ul#{ " class=\"" + options[:class].to_s + "\"" if options[:class] }>"
      end
      

      path.each do |p|
        title = p.send(options[:field].to_sym)
        title ||= p[:title]
        title ||= p[:slug]
        next if p.status.to_s != "live"
        next if options[:exclude].include? p.url
        url = p._redirect
        url ||= p.url        
        ss += "<li><a href=\"#{url}\">#{title}</a>"
        begin
          opt = options.clone
          opt[:class]   = nil unless opt[:class].is_a?(Hash)
          opt[:self]    = false
          opt[:levels] -= 1
          opt[:level]   = options[:level] + 1
          if p.path != "" && p.status.to_s == "live"
            ss += nav(p.path, opt) if opt[:levels] > 0
          end
        rescue
        end
        ss += "</li>" # use call to execute the block
      end
      ss += "</ul>"

      ss.html_safe
    
    end
  end

  def breadcrumbs(path = nil, options = {}, &block_argument)
    #path = entries(path) unless path.is_a? Array
    if block_argument # block is given
      path.each do |p|
        block_argument.call(p) # use call to execute the block
      end
    else # the value of block_argument becomes nil if you didn't give a block
      options[:levels]   ||= 1
      options[:level]    ||= 1
      options[:exclude]  ||= []
      options[:include_self]  ||= true if options[:include_self] != false
      options[:field]    ||= :title
      options[:exclude]    = [options[:exclude]] if options[:exclude] && !options[:exclude].is_a?(Array)

      if options[:class].is_a?(Array)
        ss = "<ul#{ " class=\"" + options[:class].join(" ") + "\"" if options[:class] }>"
      elsif options[:class].is_a?(Hash)
        
        if options[:class][options[:level]]
          if options[:class][options[:level]].is_a?(Array)
            ss = "<ul#{ " class=\"" + options[:class][options[:level]].join(" ") + "\"" if options[:class][options[:level]] }>"
          else
            ss = "<ul#{ " class=\"" + options[:class][options[:level]].to_s + "\"" if options[:class][options[:level]] }>"
          end
        else
          ss = "<ul>"
        end
      else
        ss = "<ul#{ " class=\"" + options[:class].to_s + "\"" if options[:class] }>"
      end

      pp = path
      last = false
      first = true
      current = true
      lis = []
      while pp do
        p = pp
        title = p.send(options[:field].to_sym)
        title ||= p.title
        title ||= p.slug

        if !current || options[:include_self] != false
          classes = []
          classes << options[:include_self]
          classes << "current" if current
          classes << "first" if first
          first = false
          classes << "inactive" if p._sub || p._redirect

          sst = "<li data-page-path=\"#{p.url}\" class=\"#{classes.join(" ")}\"><a href=\"#{p.url}\">#{title}</a>"
          begin
            opt = options.clone
            opt[:class]   = nil unless opt[:class].is_a?(Hash)
            opt[:self]    = false
            opt[:levels] -= 1
            opt[:level]   = options[:level] + 1
            if p.path != ""
              sst += nav(p.path, opt) if opt[:levels] > 0
            end
          rescue
          end
          sst += "</li>" # use call to execute the block
          lis << sst
        end
        begin
          pp = pp.parent
        rescue
          pp = nil
        end
        current = false
        pp = nil if last
        last = true if pp && pp.path == ""
      end


      ss += lis.reverse.join + "</ul>"

      ss.html_safe
    
    end
  end

  private

  def current_page
    page_finder.find
  end

  def page_finder
    page_finder_factory.new(params[:id])
  end

  def page_finder_factory
    StaticFinder
  end

  def layout_for_page
    case params[:id]
    when 'home'
      'application'
    else
      'application'
    end
  end


end
