class ContentController < ApplicationController
  
  def show
    @page = page_finder.get

    if @page._redirect
      redirect_to ("/" + @page._redirect).gsub(/^\/\//,"/")
    else
      render :template => current_page
    end    
  end

end